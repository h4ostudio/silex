<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

//$app['debug']=true;

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../views',
));

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

$app->register(new Silex\Provider\SessionServiceProvider());

$app->register(new Silex\Provider\ValidatorServiceProvider());

$app->register(new Silex\Provider\FormServiceProvider());

$app->register(new Silex\Provider\SwiftmailerServiceProvider());

$app->register(new Silex\Provider\MonologServiceProvider(), array(
            'monolog.logfile' => __DIR__.'/../log/dev.log',
));

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array (
            'driver'    => 'pdo_mysql',
            'host'      => 'mysql_read.someplace.tld',
            'dbname'    => 'my_database',
            'user'      => 'my_username',
            'password'  => 'my_password',
        )));

$app->register(new Tobiassjosten\Silex\Provider\FacebookServiceProvider(), array(
        'facebook.app_id'     => '574966375862156',
        'facebook.secret'     => '35eb1ad1871530169c68ddca835236e8',
    ));



return $app;
