<?php

$app=require_once __DIR__.'/init.php';

$options = array('scope'=>array('publish_stream'),redirect_uri=>'http://silex.dev/');

// Define error management and before filters here
$app->error(function(\Exception $e) use($app){
    return $app['twig']->render('error.html.twig',array('error'=>$e));
    });

$app->before(function() use($app, $options) {
    $app['twig']->addGlobal('facebook_login_url',$app['facebook']->getLoginUrl($options));
    $app['twig']->addGlobal('facebook_logout_url',$app['facebook']->getLogoutUrl());
    
   try
   {
        $user = $app['facebook']->getUser();
        $app['twig']->addGlobal('user',$user);
        
        if (!$user)
        {            
            if($app['request']->getRequestUri()!='/facebook-login')
            {
                $app['monolog']->addDebug('Facebook user not connected, redirect.');
                return $app->redirect('/facebook-login');               
            }
        }
   }
   catch (\FacebookApiException $e)
   {        
        if($app['request']->getRequestUri()!='/facebook-login')
        {
            $app['monolog']->addDebug('Facebook Exception, redirect.');
            return $app->redirect('/facebook-login');            
        }
   }
});

// Define facebook login route here

$app->match('/facebook-login',function() use($app){
    return $app['twig']->render('facebook-login.html.twig');    
    });

return $app;