<?php

$app = require_once __DIR__.'/../src/app.php';

//Define your app routes here

$app->match('/',function() use($app){
   return $app['twig']->render('home.html.twig',array('me'=>$app['facebook']->api('/me','GET')));
})->bind('homepage');

$app->match('/amigos',function() use($app){
    return $app['twig']->render('friends.html.twig',array(
            'friends'=>$app['facebook']->api('/me/friends','GET')
        ));
    });
$app->run();